# Kingpin map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Kingpin.

This gamepack is based on the game pack provided by Kingpin:

- http://download.kingpin.info/kingpin/editing/maps/map_editors/NetRadiant/addon/Kingpinpack.zip

Quote from initial `readme.txt` file:

FREDZ v1.0

- v1.1: Updated def file.
- v1.0: First version
